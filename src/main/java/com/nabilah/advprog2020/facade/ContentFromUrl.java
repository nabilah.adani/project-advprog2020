package com.nabilah.advprog2020.facade;

import com.nabilah.advprog2020.model.User;
import com.nabilah.advprog2020.repository.UserDatabase;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ContentFromUrl implements Facade {
    private String url;
    public UserDatabase userService;

    public ContentFromUrl(){

    }

    public ContentFromUrl(String url){
        this.url = url;
    }

    public List<String> getContentWeather(String lat, String lon, String senderId){
        try {
            ReadJson jsonWeather = new ReadJson();
            JSONObject json = jsonWeather.readJsonFromUrl("http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid=0275849977025543d3b3beea9b4509cb");
            String city = json.getString("name");
            String country = json.getJSONObject("sys").getString("country");
            List<String> nameCountry = new ArrayList<String>();
            nameCountry.add(country);
            if (country.equals("ID")){
                nameCountry.set(0, "Indonesia");
            }
            String nCountry = nameCountry.get(0);
            String weather = json.getJSONArray("weather").getJSONObject(0).getString("main");
            int humidity = json.getJSONObject("main").getInt("humidity");
            String hum = String.valueOf(humidity);
            List<User> user = userService.getConfigureUnit(senderId);
            String windUnit = user.get(0).getWindUnit();
            String tempUnit = user.get(0).getTempUnit();
            int windS = (int) (json.getJSONObject("wind").getDouble("speed"));
            String windSpeed = String.valueOf(windS);
            int temp = (int) (json.getJSONObject("main").getDouble("temp"));
            String temperature = String.valueOf(temp);

            List<String> contentWeather = new ArrayList<String>();
            contentWeather.add(0, city + ", " + nCountry);
            contentWeather.add(1, weather);
            contentWeather.add(2, windSpeed + " "  + windUnit);
            contentWeather.add(3, temperature + " " + tempUnit);
            contentWeather.add(4, hum);
            return contentWeather;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getXkcdUrl() throws IOException {
        ReadPage contentXkcd = new ReadPage();
        String html = contentXkcd.getURLSource(this.url);
        int firstIndex = html.indexOf("Image URL (for hotlinking/embedding):");
        String imageUrl = html.substring(firstIndex + 38);
        int lastIndex = imageUrl.indexOf("<");
        String previewImage = imageUrl.substring(0, lastIndex);
        return previewImage;
    }
}
