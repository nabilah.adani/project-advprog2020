package com.nabilah.advprog2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectAdvprog2020Application {

    public static void main(String[] args){
        SpringApplication.run(ProjectAdvprog2020Application.class, args);
    }
}

