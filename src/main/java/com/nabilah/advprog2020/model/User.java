package com.nabilah.advprog2020.model;

public class User {
    private Long id;
    private String userId;
    private String displayName;
    private String windUnit;
    private String tempUnit;

    public User(Long id, String userId, String displayName, String windUnit, String tempUnit) {
        this.id = id;
        this.userId = userId;
        this.displayName = displayName;
        this.windUnit = windUnit;
        this.tempUnit = tempUnit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getWindUnit() { return windUnit; }

    public void setWindUnit(String windUnit) { this.windUnit = windUnit; }

    public String getTempUnit() { return tempUnit; }

    public void setTempUnit(String tempUnit) { this.tempUnit = tempUnit; }
}
