package com.nabilah.advprog2020.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.client.LineSignatureValidator;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.ReplyEvent;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.message.*;
import com.linecorp.bot.model.objectmapper.ModelObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.nabilah.advprog2020.service.BotService;
import com.nabilah.advprog2020.model.EventsModel;

import java.io.IOException;

@RestController
public class Controller {

    @Autowired
    private BotService botService;

    @Qualifier("lineMessagingClient")
    private LineMessagingClient lineMessagingClient;

    @Autowired
    @Qualifier("lineSignatureValidator")
    private LineSignatureValidator lineSignatureValidator;

    @RequestMapping(value="/webhook", method= RequestMethod.POST)
    public ResponseEntity<String> callback(
            @RequestHeader("X-Line-Signature") String xLineSignature,
            @RequestBody String eventsPayload)
    {
        try {
            if (!lineSignatureValidator.validateSignature(eventsPayload.getBytes(), xLineSignature)) {
                throw new RuntimeException("Invalid Signature Validation");
            }
            handleEvent(eventsPayload, xLineSignature);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    public void handleEvent(String eventsPayload, String xlineSignature)
            throws JsonProcessingException {
        ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
        EventsModel eventsModel = objectMapper.readValue(eventsPayload, EventsModel.class);

        eventsModel.getEvents().forEach((event) -> {
            if (event instanceof FollowEvent) {
                String replyToken = ((ReplyEvent) event).getReplyToken();
                botService.source = event.getSource();
                botService.greetingMessage(replyToken);
            } else if (event instanceof MessageEvent) {
                botService.source = event.getSource();
                if (((MessageEvent) event).getMessage() instanceof TextMessageContent) {
                    botService.handleMessageEvent((MessageEvent) event);
                } else if (((MessageEvent) event).getMessage() instanceof LocationMessageContent) {
                    botService.handleLocationEvent((MessageEvent) event);
                }
            }
        });
    }
}