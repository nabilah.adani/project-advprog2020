package com.nabilah.advprog2020.database;

import com.nabilah.advprog2020.model.User;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

public class UserDaoImplementation implements UserDao {
    private final static String USER_TABLE="tbl_user";
    private static final String SQL_SELECT_ALL = "SELECT * FROM " + USER_TABLE;
    private static final String SQL_GET_BY_USER_ID = SQL_SELECT_ALL
            + " WHERE LOWER(userid) LIKE LOWER(?);";
    private static final String SQL_REGISTER = "INSERT INTO "
            + USER_TABLE + " (userid, displayname, windunit, tempunit) VALUES (?, ?, ?, ?);";
    private static final String SQL_UPDATE_WEATHER = "UPDATE " + USER_TABLE
            + " SET windunit = ?, tempunit = ? WHERE LOWER(userid) LIKE LOWER(?);";
    private static final String SQL_GET_WEATHER = SQL_SELECT_ALL + " WHERE LOWER(userid) LIKE LOWER(?);";
    private static final ResultSetExtractor<List<User>> MULTIPLE_RS_EXTRACTOR =
            UserDaoImplementation::extractData;

    private JdbcTemplate mJdbc;

    public UserDaoImplementation(DataSource dataSource) {
        mJdbc = new JdbcTemplate(dataSource);
    }

    public static List<User> extractData(ResultSet resultSet) throws SQLException {
        List<User> list = new Vector<User>();
        while (resultSet.next()) {
            User p = new User(
                    resultSet.getLong("id"),
                    resultSet.getString("userId"),
                    resultSet.getString("displayName"),
                    resultSet.getString("windunit"),
                    resultSet.getString("tempunit"));
            list.add(p);
        }
        return list;
    }

    @Override
    public List<User> get() {
        return mJdbc.query(SQL_SELECT_ALL, MULTIPLE_RS_EXTRACTOR);
    }

    @Override
    public List<User> getByUserId(String userId) {
        return mJdbc.query(SQL_GET_BY_USER_ID, new Object[]{"%" + userId + "%"},
                MULTIPLE_RS_EXTRACTOR);
    }

    @Override
    public int registerUser(String userId, String displayName) {
        String windUnit = " ";
        String tempUnit = " ";
        return mJdbc.update(SQL_REGISTER, userId, displayName, windUnit, tempUnit);
    }

    @Override
    public int setConfigureWeather(String windUnit, String tempUnit, String userId){
        return mJdbc.update(SQL_UPDATE_WEATHER, windUnit, tempUnit, "%" + userId + "%");
    }

    @Override
    public List<User> getConfigureWeather(String userId) {
        return mJdbc.query(SQL_GET_WEATHER, new Object[]{"%" + userId + "%"},
                MULTIPLE_RS_EXTRACTOR);
    }
}
