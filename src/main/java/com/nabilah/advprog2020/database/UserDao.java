package com.nabilah.advprog2020.database;

import com.nabilah.advprog2020.model.User;
import java.util.List;

public interface UserDao {
    public List<User> get();
    public List<User> getByUserId(String aUserId);
    public int registerUser(String aUserId, String aDisplayName);
    public int setConfigureWeather(String windUnit, String tempUnit, String userId);
    public List<User> getConfigureWeather(String userId);
}
