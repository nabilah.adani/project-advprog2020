package com.nabilah.advprog2020.repository;

import com.nabilah.advprog2020.model.User;
import com.nabilah.advprog2020.database.UserDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDatabase {

    @Autowired
    private UserDao userDao;

    public int registerUser(String userId, String displayName) {
        if (findUser(userId) == null) {
            return userDao.registerUser(userId, displayName);
        }
        return -1;
    }

    public String findUser(String aUserId){
        List<User> self=userDao.getByUserId("%"+aUserId+"%");

        if(self.size() > 0)
        {
            return self.get(0).getUserId();
        }

        return null;
    }

    public int setConfigureUnit(String windUnit, String tempUnit, String userId){
        return userDao.setConfigureWeather(windUnit, tempUnit, userId);
    }

    public List<User> getConfigureUnit(String userId){
        return userDao.getConfigureWeather(userId);
    }
}
