package com.nabilah.advprog2020.service;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.LocationMessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.ImageMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;
import com.nabilah.advprog2020.repository.UserDatabase;
import com.nabilah.advprog2020.facade.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class BotService {

    public Source source;
    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private UserDatabase userService;

    @Autowired
    private BotTemplate botTemplate;

    public void greetingMessage(String replyToken) {
        registerUser();
        replyFlexMenu(replyToken);
    }

    private void registerUser() {
        String senderId = source.getSenderId();
        UserProfileResponse sender = getProfile(senderId);
        userService.registerUser(sender.getUserId(), sender.getDisplayName());
    }

    public void replyImage(String replyToken, String idComic) {
        try {
            testNumber(idComic, replyToken);
            String pageUrl = "https://xkcd.com/" + idComic;
            Facade contentImage = new ContentFromUrl(pageUrl);
            String previewImage = ((ContentFromUrl) contentImage).getXkcdUrl();
            ImageMessage imageMessage = new ImageMessage(pageUrl, previewImage);
            reply(replyToken, imageMessage);
        } catch (IOException | RuntimeException e ) {
            e.printStackTrace();
            replyText(replyToken, "Gambar tidak tersedia. \r\nSilahkan masukkan kembali id, dengan angka diantara 1 - 2340.");
        }
    }

    public void replyConfigure(String replyToken, String userConfigure){
        int lastIndex = userConfigure.indexOf(" ");
        String tempUnit = userConfigure.substring(lastIndex + 1);
        String windUnit = userConfigure.substring(0, lastIndex);
        String userId = source.getSenderId();
        if (isNumber(windUnit) && isNumber(tempUnit)){
            replyText(replyToken, "Tolong masukkan kembali input. \r\nInput tidak bisa angka.\r\nFormat tetap sama yaitu : \r\n/configure_weather <wind_unit> <temp_unit>");
        } else if (isNumber(windUnit)) {
            replyText(replyToken, "Tolong masukkan kembali wind unit. \r\nInput tidak bisa angka.\r\nFormat tetap sama yaitu : \r\n/configure_weather <wind_unit> <temp_unit>");
        } else if (isNumber(tempUnit)){
            replyText(replyToken, "Tolong masukkan kembali temperature unit. \r\nInput tidak bisa angka.\r\nFormat tetap sama yaitu : \r\n/configure_weather <wind_unit> <temp_unit>");
        } else {
            userService.setConfigureUnit(windUnit, tempUnit, userId);
            replyText(replyToken, "konfigurasi telah diterima");
        }
    }

    public void replyOtherInput(String replyToken){
        UserProfileResponse sender = getProfile(source.getSenderId());
        FlexMessage flexMessage = botTemplate.createFlexMenu();
        List<Message> messageList = new ArrayList<>();
        messageList.add(new TextMessage("Terima kasih telah mengirimkan pesan, " + sender.getDisplayName() + "!\r\nApa yang ingin kamu lakukan?" ));
        messageList.add(flexMessage);
        reply(replyToken, messageList);
    }

    public void replyFlexMenu(String replyToken) {
        String senderId = source.getSenderId();
        UserProfileResponse sender = getProfile(senderId);

        FlexMessage flexMessage = botTemplate.createFlexMenu();
        List<Message> messageList = new ArrayList<>();
        messageList.add(new TextMessage("Hi "
                + sender.getDisplayName() + ", apa yang ingin kamu lakukan ?"));
        messageList.add(flexMessage);
        reply(replyToken, messageList);
    }

    public void replyText(String replyToken, String message){
        TextMessage textMessage=new TextMessage(message);
        reply(replyToken, textMessage);
    }

    public void reply(String replyToken, Message message) {
        ReplyMessage replyMessage=new ReplyMessage(replyToken, message);
        reply(replyMessage);
    }

    public void reply(String replyToken, List<Message> message) {
        ReplyMessage replyMessage = new ReplyMessage(replyToken, message);
        reply(replyMessage);
    }

    private void reply(ReplyMessage replyMessage) {
        try {
            lineMessagingClient.replyMessage(replyMessage).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public UserProfileResponse getProfile(String userId) {
        try {
            return lineMessagingClient.getProfile(userId).get();
        } catch (InterruptedException | ExecutionException | RuntimeException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void handleMessageEvent(MessageEvent messageEvent) {
        TextMessageContent textMessageContent = (TextMessageContent) messageEvent.getMessage();
        String replyToken = messageEvent.getReplyToken();
        String userMessage = textMessageContent.getText();
        if (userMessage.toLowerCase().equals("/weather")) {
            replyText(replyToken, "Bagikan Lokasi anda");
        } else if (userMessage.toLowerCase().equals("/xkcd")) {
            replyText(replyToken, "Silahkan ketikkan kembali /xkcd kemudian diikuti id gambar. \r\n" + "contoh : /xkcd 150");
        } else if (userMessage.toLowerCase().contains("/xkcd")) {
            String idComic = userMessage.substring(6);
            replyImage(replyToken, idComic);
        } else if (userMessage.toLowerCase().contains("/configure_weather")) {
            String userConfigure = userMessage.substring(19);
            replyConfigure(replyToken, userConfigure);
        } else {
            replyOtherInput(replyToken);
        }
    }

    public void handleLocationEvent(MessageEvent messageEvent) {
        LocationMessageContent locationMessageContent=(LocationMessageContent) messageEvent.getMessage();
        String replyToken=messageEvent.getReplyToken();
        Double x = locationMessageContent.getLatitude();
        Double y = locationMessageContent.getLongitude();
        String lat = Double.toString(x);
        String lon = Double.toString(y);
        String senderId = source.getSenderId();
        Facade contentWeather = new ContentFromUrl();
        List<String> contentReplyWeather = ((ContentFromUrl) contentWeather).getContentWeather(lat, lon, senderId);
        String replyWeather = "Weather at your position (" + contentReplyWeather.get(0) + "):\r\n"
                + contentReplyWeather.get(1) + "\r\n"
                + contentReplyWeather.get(2)+ "\r\n"
                + contentReplyWeather.get(3) + "\r\n"
                + contentReplyWeather.get(4) + "%";
        replyText(replyToken, replyWeather);
    }



    static boolean isNumber(String s) {
        for (int i = 0; i < s.length(); i++)
            if (Character.isDigit(s.charAt(i))
                    == false)
                return false;

        return true;
    }

    private void testNumber(String idComic, String replyToken) {
        try {
            int testNumber = Integer.parseInt(idComic);
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            replyText(replyToken, "Input bukan angka, tolong masukkan input dalam angka");
        }
        return;
    }


    public boolean condition() {
        return false;
    }
}


