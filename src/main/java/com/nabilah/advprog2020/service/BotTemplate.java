package com.nabilah.advprog2020.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.FlexContainer;
import com.linecorp.bot.model.objectmapper.ModelObjectMapper;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BotTemplate {
    @Autowired
    BotService botService;

    public FlexMessage createFlexMenu() {
        FlexMessage flexMessage = new FlexMessage("Action Menu", null);
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            String encoding = StandardCharsets.UTF_8.name();
            String flexTemplate = IOUtils.toString(Objects.requireNonNull(
                    classLoader.getResourceAsStream("menu.json")), encoding);
            ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
            objectMapper = botService.condition() ? null : objectMapper;
            FlexContainer flexContainer = objectMapper.readValue(flexTemplate, FlexContainer.class);
            flexMessage = new FlexMessage("Action Menu", flexContainer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flexMessage;
    }
}
